package ru.t1.kharitonova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.Task;

@NoArgsConstructor
public final class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
