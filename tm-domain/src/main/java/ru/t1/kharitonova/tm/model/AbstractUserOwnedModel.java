package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    public AbstractUserOwnedModel(@NotNull final String userId) {
        this.userId = userId;
    }

}
