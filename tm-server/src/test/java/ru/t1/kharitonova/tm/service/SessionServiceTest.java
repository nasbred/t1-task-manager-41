package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.model.User;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private List<User> userList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userAdmin);
            userList.add(userAdmin);
        } else {
            userList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userTest);
            userList.add(userTest);
        } else {
            userList.add(userService.findByLogin("test"));
        }

        @Nullable final String adminUserId = userAdmin.getId();
        @Nullable final String testUserId = userTest.getId();
        @NotNull final Session sessionAdmin = new Session(adminUserId, Role.ADMIN);
        @NotNull final Session sessionTest = new Session(testUserId, Role.USUAL);

        if (sessionService.findOneByUserId(adminUserId) == null) {
            sessionList.add(sessionService.create(sessionAdmin));
        } else {
            sessionList.add(sessionService.findOneByUserId(adminUserId));
        }
        if (sessionService.findOneByUserId(testUserId) == null) {
            sessionList.add(sessionService.create(sessionTest));
        } else {
            sessionList.add(sessionService.findOneByUserId(testUserId));
        }
    }

    @After
    public void clear() {
        sessionService.removeAll();
        sessionList.clear();
        userList.clear();
    }

    @Test
    public void testCreate() {
        final int expectedSize = sessionList.size();
        Assert.assertEquals(expectedSize, sessionService.getSize());
        @NotNull final Session session = new Session(userList.get(1).getId(), Role.USUAL);
        sessionService.create(session);
        Assert.assertEquals(expectedSize + 1, sessionService.getSize());
    }


    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            Assert.assertTrue(sessionService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(sessionService.existsById("Other_Id"));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            @Nullable final Session session1 = sessionService.findOneById(id);
            Assert.assertNotNull(session1);
            Assert.assertEquals(id, session1.getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final Session session = sessionService.findOneById(id);
        Assert.assertNull(session);
    }

    @Test
    public void testGetSize() {
        final int projectSize = sessionService.getSize();
        Assert.assertEquals(sessionList.size(), projectSize);
    }

    @Test
    public void testRemoveById() {
        for (int i = 0; i < sessionList.size(); i++) {
            @NotNull final Session session = sessionList.get(i);
            sessionService.removeById(session.getId());
            Assert.assertEquals(sessionService.getSize(), sessionList.size() - i - 1);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String id = "Other_Id";
        sessionService.removeById(id);
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testRemoveAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

}
