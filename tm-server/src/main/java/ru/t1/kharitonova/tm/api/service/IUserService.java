package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserService {

    @Nullable
    User add(@NotNull User model);

    @NotNull
    Collection<User> addAll(@NotNull Collection<User> models);

    @NotNull
    Collection<User> set(@NotNull Collection<User> models);

    @Nullable
    List<User> findAll();

    @Nullable
    List<User> findAll(@NotNull Comparator<User> comparator);

    @Nullable
    User findOneById(@NotNull String id);

    @Nullable
    User findOneByIndex(@NotNull Integer index);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String login);

    int getSize();

    boolean existsById(@NotNull String id);

    @Nullable
    User remove(@Nullable User model);

    void removeOneById(@NotNull String id);

    @Nullable
    User removeOneByIndex(@NotNull Integer index);

    void removeAll(@NotNull List<User> models);

    void removeAll();

    void update(@NotNull User model);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

    @NotNull
    default Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    default Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}
