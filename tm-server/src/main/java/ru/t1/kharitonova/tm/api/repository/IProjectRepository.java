package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, description, status, created, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{userId});")
    void add(@NotNull Project project);

    default Collection<Project> addAll(@NotNull final Collection<Project> models) {
        @NotNull List<Project> result = new ArrayList<>();
        for (@NotNull final Project model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @Update("UPDATE tm_project SET status = #{status} WHERE id = #{id} AND user_id = #{userId};")
    void changeStatusById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id,
            @Nullable @Param("status") Status status
    );

    default boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Select("SELECT * FROM tm_project;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<Project> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<Project> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    Project findOneById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{user_id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    Project findOneByIdAndUserId(@Param("user_id") @NotNull String user_id, @Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_project OFFSET #{index} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    Project findOneByIndex(@Param("index") @NotNull Integer index);

    @Select("SELECT * FROM tm_project WHERE user_id = #{user_id} OFFSET #{index} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    Project findOneByIndexByUserId(
            @NotNull @Param("user_id") String user_id,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(1) FROM tm_project;")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{user_id};")
    int getSizeForUser(@NotNull @Param("user_id") String user_id);

    default void remove(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        removeOneById(model.getId());
    }

    @Delete("DELETE FROM tm_project WHERE id = #{id};")
    void removeOneById(@NotNull @Param("id") String id);

    default Project removeOneByIndex(@NotNull final Integer index) {
        @Nullable final Project model = findOneByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    default Project removeOneByIndexForUser(@NotNull String userId, @NotNull final Integer index) {
        @Nullable final Project model = findOneByIndexByUserId(userId, index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Delete("TRUNCATE TABLE tm_project;")
    void removeAll();

    default void removeAllByList(@NotNull List<Project> projects) {
        for (@NotNull final Project project : projects) remove(project);
    }

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    default Collection<Project> set(@NotNull final Collection<Project> models) {
        removeAll();
        return addAll(models);
    }

    @Update("UPDATE tm_project SET created = #{created}, name = #{name}, description = #{description}, " +
            "status = #{status}, user_id = #{userId} WHERE id = #{id};")
    void update(@NotNull Project model);

}
