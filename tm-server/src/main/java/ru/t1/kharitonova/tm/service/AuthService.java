package ru.t1.kharitonova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.ISessionService;
import ru.t1.kharitonova.tm.api.service.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.exception.user.LoginEmptyException;
import ru.t1.kharitonova.tm.exception.user.PasswordEmptyException;
import ru.t1.kharitonova.tm.exception.user.SessionTimeoutException;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.CryptUtil;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(@NotNull final IPropertyService propertyService,
                       @NotNull final IUserService userService,
                       @NotNull ISessionService sessionService) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Nullable
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public @NotNull User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null || user.isLocked()) throw new AccessDeniedException();
        @Nullable String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

    @NotNull
    protected Session createSession(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.create(session);
    }

    @Override
    public @NotNull String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (user == null || user.isLocked() || hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @SneakyThrows
    @NotNull
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @SneakyThrows
    @Override
    public Session validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);

        @NotNull Date currentDate = new Date();
        @NotNull Date sessionDate = session.getCreated();
        final int timeout = propertyService.getSessionTimeout();
        if ((currentDate.getTime() - sessionDate.getTime()) / 1000 > timeout) {
            throw new SessionTimeoutException();
        }
        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable final Session session) {
        if (session == null) return;
        sessionService.removeById(session.getId());
    }

}
