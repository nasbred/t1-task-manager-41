package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.Session;

public interface ISessionService {

    @NotNull
    Session create(@Nullable Session session);

    Boolean existsById(@Nullable String id);

    @Nullable
    Session findOneById(@Nullable String id);

    @Nullable
    Session findOneByUserId(@Nullable String id);

    void removeById(@Nullable String id);

    void removeAll();

    int getSize();

}
