package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    Session validateToken(@Nullable String token);

    void invalidate(Session session);

}
